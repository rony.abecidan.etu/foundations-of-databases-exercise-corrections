# Exercise 4.2

(a) $`q`$ is a rule so is range restricted. By definition, if $`\mathcal{I}`$ is an instance over $`\mathcal{R}`$, 

$`q(\mathcal{I}) = \{ v(u_i) | v \text{ is a valuation over } var(q) \text{ and } v(u_i) \in \mathcal(I)(R_i), i \in [1;n] \}`$

Valuations map terms (tuples with elements in  $`dom \cup var`$) to tuples only with elements from $`dom`$. Then, all constantes in $`v(u_i)`$ are either in $`adom(q)`$, or in $`adom(\mathcal{I})`$. 
So, 

$`q(\mathcal{I}) = adom(q(\mathcal{I})) \subseteq admon(q) \cup admon(\mathcal{I}) = admon(q,\mathcal{I})`$

$`\mathcal{I}`$ is an instance so $`admon(\mathcal{I})`$ is finite, and $`q`$ is a rule so $`admon(q)`$ is also finite. Finally, $`q(\mathcal{I})`$ is finite.

(b) Let $`\mathcal{I}`$ is an instance over $`\mathcal{R}`$, let $`n`$ be the number of tuples in $`\mathcal{I}`$, let $`m`$ be the arity of $`q`$. Then the upper bound of the number of tuples that can occur in $`q(\mathcal{I})`$ is $`n^m`$. This upper bound is achieved when we query all variables separately in every relation instance. For example, if the instance is a single relation called M with arity 3, the query: $`ans(x, y, z) \leftarrow M(x, y_1, z_1), M(x_2, y, z_2), M(x_3, y_3, z)`$ achieve the upper bound $`n^3`$.


# Exercise 4.1:

#### (4.1) 

* $`ans(x_d) \leftarrow Movies(\text{"Cries and Whispers"}, x_d, x_a)`$
* | Movies | Title                | Director | Actor   |
  |--------|----------------------|----------|---------|
  |        | "Cries and Whispers" | $`x_d`$  | $`x_a`$ |

  $`(\textbf{T}, <x_d, x_a>)`$
* $`\{ x_d | \, \exists x_a, \, Movies(\text{"Cries and Whispers"}, x_d, x_a) \}`$
* $`\pi_2(\sigma_{1=\text{"Cries and Whispers"}}(Movies))`$
* $`\pi_{\text{Director}}(\sigma_{\text{Title}=\text{"Cries and Whispers"}}(Movies))`$

#### (4.2)

* $`ans(x_{th}) \leftarrow Pariscope(x_{th}, \text{"Cries and Whispers"}, x_s)`$
* | Pariscope | Theater | Title         | Schedule|
  |-----------|------------|---------------|---------|
  |           | $`x_{th}`$  | "Cries and Whispers"  | $`x_s`$ |

 $`(\textbf{T}, <x_{th}, x_s>)`$
* $`\{ x_{th} | \, \exists x_s, \, Pariscope(x_{th}, \text{"Cries and Whispers"}, x_s) \}`$
* $`\pi_1(\sigma_{2=\text{"Cries and Whispers"}}(Pariscope))`$
* $`\pi_{\text{Theater}}(\sigma_{\text{Title}=\text{"Cries and Whispers"}}(Pariscope))`$

#### (4.3)

* $`ans(x_{ad}, x_p) \leftarrow Location(\text{"Le Champo"}, x_{ad}, x_p)`$
* | Location | Theater | Address         | Phone Number|
  |-----------|------------|---------------|---------|
  |           | "Le Champo"  |  $`x_{ad}`$ | $`x_p`$ |

  $`(\textbf{T}, <x_{ad}, x_p>)`$
* $`\{ x_{ad}, x_p | \, Location(\text{"Le Champo"}, x_{ad}, x_p) \}`$
* $`\pi_{2,3}(\sigma_{1=\text{"Le Champo"}}(Location))`$
* $`\pi_{\text{Address, Phone Number}}(\sigma_{\text{Theater}=\text{"Le Champo"}}(Location))`$

#### (4.5)

* $`ans() \leftarrow Movies(x_t, \text{"Bergman"}, x_a), Pariscope(x_{th}, x_t, x_s)`$
* | Movies | Title                | Director | Actor   |
  |--------|----------------------|----------|---------|
  |        | $`x_t`$ | Bergman  | $`x_a`$ |

   Pariscope | Theater | Title         | Schedule|
  |-----------|------------|---------------|---------|
  |           | $`x_{th}`$  | $`x_t`$ | $`x_s`$ |

  $`(\textbf{T}, <>)`$

* $`\{<>| \, \exists x_t, x_a, x_{th}, x_s \, Movies(x_t, \text{"Bergman"}, x_a), Pariscope(x_{th}, x_t, x_s) \}`$
* $`\pi_{\varnothing}(\sigma_{2=\text{"Bergman"}}(Movies) \times Pariscope)`$
* $`\pi_{\varnothing}(\sigma_{\text{Director}=\text{"Bergman"}}(Movies) \bowtie_{\text{Title}} Pariscope)`$

#### (4.6)

* $`ans(x_d, x_a) \leftarrow Movies(x_t, x_d, x_a), Movies(x'_t, x_a, x_d)`$
* | Movies | Title                | Director | Actor   |
  |--------|----------------------|----------|---------|
  |        | $`x_t`$ |  $`x_d`$ | $`x_a`$ |
  |        | $`x'_t`$ |  $`x_a`$ | $`x_d`$ |


  $`(\textbf{T}, <x_d, x_a>)`$

* $`\{x_d, x_a| \, \exists x_t, x'_t \, Movies(x_t, x_d, x_a), Movies(x'_t, x_a, x_d) \}`$
* $`\pi_{2,3}(\sigma_{2=6, 3=5}(Movies \times Movies))`$
* $`\pi_{\text{Director, Actor}}(Movies \bowtie_{\text{Director}=p_2,\text{Actor}=p_1}(\delta_{\text{Director, Actor}\rightarrow p_1, p_2}(Movies)))`$

#### (4.7)

* $`ans(x_d) \leftarrow Movies(x_t, x_d, x_d)`$
* | Movies | Title                | Director | Actor   |
  |--------|----------------------|----------|---------|
  |        | $`x_t`$ |  $`x_d`$ | $`x_d`$ |

  $`(\textbf{T}, <x_d>)`$

* $`\{x_d| \, \exists x_t \, Movies(x_t, x_d, x_d) \}`$
* $`\pi_{2}(\sigma_{2=3}(Movies))`$
* $`\pi_{\text{Director}}(\sigma_{\text{Director} = \text{Actor}}(Movies))`$

#### (4.8)

* $`ans(x_1, x_2) \leftarrow Movies(x_t, x_d, x_1), Movies(x_t, x_d, x_2)`$
* | Movies | Title                | Director | Actor   |
  |--------|----------------------|----------|---------|
  |        | $`x_t`$ |  $`x_d`$ | $`x_1`$ |
  |        | $`x_t`$ |  $`x_d`$ | $`x_2`$ |

  $`(\textbf{T}, <x_1, x_2>)`$

* $`\{x_1, x_2| \, \exists x_t, x_d \, Movies(x_t, x_d, x_1), Movies(x_t, x_d, x_2) \}`$
* $`\pi_{3,6}(\sigma_{1=4,2=5}(Movies \times Movies))`$
* $`\pi_{\text{Actor, Other Actor}}(\sigma_{\text{Title}=\text{Other Title}, \text{Director}=\text{Other Director}}(Movies \times \delta_{\text{Title, Director, Actor} \rightarrow \text{Other Title, Other Director, Other Actor}}(Movies)))`$

#### (4.9)

* $`ans(\text{"Apocalypse Now"}, \text{"Coppola"})`$
* $`(, <\text{"Apocalypse Now"}, \text{"Coppola"}>)`$

* $`\{ <\text{"Apocalypse Now"}, \text{"Coppola"}> \}`$
* $`<\text{"Apocalypse Now"}, \text{"Coppola"}>`$
* $`<\text{"Apocalypse Now"}, \text{"Coppola"}>)`$

